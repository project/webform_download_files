<?php

namespace Drupal\webform_download_files\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform_download_files\WebformSubmissionFilesDownloader;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Download all webform submissions files in one zip file.
 *
 * @Action(
 *   id = "webform_download_files_action",
 *   label = @Translation("Download files"),
 *   type = "webform_submission"
 * )
 */
class WebformDownloadFilesAction extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('webform_download_files.downloader')
    );
  }

  /**
   * The downloader service.
   *
   * @var WebformSubmissionFilesDownloader
   */
  protected $downloader;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param WebformSubmissionFilesDownloader $downloader
   *   The downloader service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, WebformSubmissionFilesDownloader $downloader) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->downloader = $downloader;
  }

  /**
   * @inheritDoc
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    // If the view can be accessed, there is no need for extra access check.
    return TRUE;
  }

  /**
   * @inheritDoc
   */
  public function execute($entity = NULL) {
    return $entity->id();
  }

  /**
   * @inheritDoc
   */
  public function executeMultiple(array $entities) {
    $sids = [];
    foreach ($entities as $entity) {
     $sids[] = $this->execute($entity);
    }
    $filename = $this->downloader->createZip($entity->getWebform()->id(), $sids);
    $this->downloader->downloadZip($filename);
  }

}
