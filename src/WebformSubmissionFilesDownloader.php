<?php

namespace Drupal\webform_download_files;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\Response;
use ZipArchive;

/**
 * Webform submission files downloader service.
 */
class WebformSubmissionFilesDownloader {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Include the messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a WebformSubmissionFilesDownloader object.
   *
   * @param FileSystemInterface $file_system
   *   The file handler.
   */
  public function __construct(FileSystemInterface $file_system, MessengerInterface $messenger) {
    $this->fileSystem = $file_system;
    $this->messenger = $messenger;
  }

  /**
   * Create zip file with all submissions attachments.
   *
   * @param $webform_id
   *   The webform ID.
   * @param $sids
   *   Array of submissions IDs.
   * @return string
   *   File name of the zip.
   */
  public function createZip($webform_id, $sids = []) {
    $private = $this->fileSystem->realpath('private://');
    $zip_folder_path = $private . '/webform_zips';
    $this->fileSystem->prepareDirectory($zip_folder_path, FileSystemInterface::CREATE_DIRECTORY);
    chdir($zip_folder_path);

    $zip = new ZipArchive();
    $filename = $webform_id . '_files.zip';
    if ((file_exists($filename) && $zip->open($filename, ZipArchive::OVERWRITE) !== TRUE) || $zip->open($filename, ZipArchive::CREATE) !== TRUE) {
      $this->messenger->addMessage("Unable to create file ($filename)", MessengerInterface::TYPE_ERROR);
      return $filename;
    }

    $webform_files_folder = $private . '/webform/' . $webform_id . '/';
    foreach ($sids as $sid) {
      $this->addToZip($zip, $webform_files_folder, $sid);
    }

    $zip->close();
    return $filename;
  }

  /**
   * Build response to download file.
   *
   * @param $filename
   *   File name of the zip.
   */
  public function downloadZip($filename) {
    if (file_exists($filename)) {
      $response = new Response();
      $response->headers->set('Content-Type', 'application/zip');
      $response->headers->set('Content-disposition', 'attachment; filename=' . basename($filename));
      $response->headers->set('Content-Transfer-Encoding', 'binary');
      $response->setContent(file_get_contents($filename));
      unlink($filename);
      $response->send();
    }
    else {
      $this->messenger->addMessage("Unable to create file ($filename)", MessengerInterface::TYPE_ERROR);
    }
  }

  /**
   * Add to zip archive all files from a webform submission folder.
   *
   * @param ZipArchive $zip
   *   The zip archive.
   * @param $base_path
   *   The base path to the webform folder.
   * @param $sid
   *   The webform submission ID.
   */
  protected function addToZip(ZipArchive $zip, $base_path, $sid) {
    $dir = $base_path . $sid . '/';
    if (is_dir($dir)) {
      if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false){
          // If file
          if (is_file($dir . $file)) {
            if ($file != '' && $file != '.' && $file != '..') {
              $zip->addFile($dir . $file, $sid . '/' . $file);
            }
          }
          else {
            // If directory
            if (is_dir($dir . $file)) {
              if ($file != '' && $file != '.' && $file != '..' && $file != '_sid_') {
                // Add empty directory
                $zip->addEmptyDir($file);
                // Read data of the folder
                $this->addToZip($zip, $dir, $file);
              }
            }
          }
        }
        closedir($dh);
      }
    }
  }

}
