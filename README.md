# Webform Download Files

Provides an action for Webform submission operations bulk form to download all
attachments together in a zip file.

## Requirements

This module requires the following modules:

- [Address](https://www.drupal.org/project/address)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](
https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Once installed you can add a "Webform submission operations bulk form" field
to a View, choosing "Download files" as "Selected actions".

## Maintainers

- Juan Olalla - [juanolalla](https://www.drupal.org/u/juanolalla)
